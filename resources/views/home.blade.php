@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="logo">
                <a href="javascript:void(0);">Face<b>book</b></a>
                <small>Publicaciones y comentarios</small>
            </div>
            <div class="card">
                <div>
                    Bienvenid@, {{ Auth::user()->name }}
                </div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    Muro!
                    <a class="btn btn-link" href="{{ route('perfil') }}">
                                        {{ __('Perfil') }}
                                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
