<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Sign Up | Bootstrap Based Admin Template - Material Design</title>
    <!-- Favicon-->
    <link rel="icon" href="../../favicon.ico" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="{{ asset('plugins/bootstrap/css/bootstrap.css') }}" rel="stylesheet">
    <!--link href="../../plugins/bootstrap/css/bootstrap.css" rel="stylesheet"-->

    <!-- Waves Effect Css -->
    <link href="{{ asset('plugins/node-waves/waves.css') }}" rel="stylesheet">
    <!--link href="../../plugins/node-waves/waves.css" rel="stylesheet" /-->

    <!-- Animation Css -->
    <link href="{{ asset('plugins/animate-css/animate.css') }}" rel="stylesheet">
    <!--link href="../../plugins/animate-css/animate.css" rel="stylesheet" /-->

    <!-- Custom Css -->
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <!--link href="../../css/style.css" rel="stylesheet"-->
</head>

<body class="signup-page">
    <div class="signup-box">
        <div class="logo">
            <a href="javascript:void(0);">Face<b>book</b></a>
            <small>Publicaciones y comentarios</small>
        </div>
        <div class="card">
            <div class="body">
                <form method="POST" action="{{ route('register') }}">
                        @csrf
                    <div class="msg">Registrar nuevo miembro</div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">person</i>
                        </span>
                        <div class="form-line">
                            <input type="text" name="name" placeholder="Nombre" required autofocus id="name" class="form-control @error('name') is-invalid @enderror" value="{{ old('name') }}" required autocomplete="name" autofocus>
                            @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                        </div>
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">email</i>
                        </span>
                        <div class="form-line">
                            <input type="email" name="email" placeholder="Email" id="email"  class="form-control @error('email') is-invalid @enderror"  value="{{ old('email') }}" required autocomplete="email">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                        </div>
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">lock</i>
                        </span>
                        <div class="form-line">
                            <input type="password" name="password" minlength="4" placeholder="Contraseña" id="password" class="form-control @error('password') is-invalid @enderror" required autocomplete="new-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                        </div>
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">lock</i>
                        </span>
                        <div class="form-line">
                            <input type="password" class="form-control" minlength="4" placeholder="Confirmar Contraseña" id="password-confirm"  name="password_confirmation" required autocomplete="new-password">
                        </div>
                    </div>
                    <button class="btn btn-block btn-lg bg-pink waves-effect" type="submit">Registrarme</button>
                    <a class="btn btn-link" href="{{ route('login') }}" >Iniciar sesión</a>
                </form>
            </div>
        </div>
    </div>

    <!-- Jquery Core Js -->
    <script src="{{ asset('plugins/jquery/jquery.min.js') }}"></script>
    <!--script src="../../plugins/jquery/jquery.min.js"></script-->

    <!-- Bootstrap Core Js -->
    <script src="{{ asset('plugins/bootstrap/js/bootstrap.js') }}"></script>
    <!--script src="../../plugins/bootstrap/js/bootstrap.js"></script-->

    <!-- Waves Effect Plugin Js -->.
    <script src="{{ asset('plugins/node-waves/waves.js') }}"></script>
    <!--script src="../../plugins/node-waves/waves.js"></script-->

    <!-- Validation Plugin Js -->
    <script src="{{ asset('plugins/jquery-validation/jquery.validate.js') }}"></script>
    <!--script src="../../plugins/jquery-validation/jquery.validate.js"></script-->

    <!-- Custom Js -->
    <script src="{{asset('js/admin.js') }}"></script>
    <!--script src="../../js/admin.js"></script-->
    <script src="{{ asset('js/pages/examples/sign-up.js') }}"></script>
    <!--script src="../../js/pages/examples/sign-up.js"></script-->
</body>

</html>