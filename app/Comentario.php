<?php

namespace Book;

use Illuminate\Database\Eloquent\Model;

class Comentario extends Model
{
    //
    protected $table = 'comentarios';

	protected $fillable = [
	        'nombre', 'email', 'password',
	];

    public function publicacion()
    {
    	return $this->belongsTo("Book\Publicacion", "id_publicacion");
    }

    public function usuario()
    {
    	return $this->belongsTo("Book\User", "id_user");
    }
}
