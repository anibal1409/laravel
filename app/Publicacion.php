<?php

namespace Book;

use Illuminate\Database\Eloquent\Model;

class Publicacion extends Model
{
    //
    protected $table = 'publicaciones';

	protected $fillable = [
	        'titulo', 'descripcion', 'id_user',
	];

    public function usuario()
    {
    	return $this->belongsTo("Book\User","id_user");
    }

    public function comentario()
    {
    	return $this->HasMany("Book\Comentario");
    }



}
